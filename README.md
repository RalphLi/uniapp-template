# 项目介绍

## 主架构 uniapp

###	介绍 
-	组件使用easycom模式 cl-*(可在pages里面修改)

###	plugins  
-	路由 ->  [uni-simple-router](https://hhyang.cn/v2/)
-	富文本 ->  [mp-html](https://ext.dcloud.net.cn/plugin?id=805)
-	ui框架 ->  [uview](http://uviewui.com/)
-	api请求 ->  [uview自带api请求](http://uviewui.com/js/http.html)
-	图标 ->  [Iconfont](https://www.iconfont.cn/)

###	项目结构

```
├─common
│  ├─config 			// 环境配置 (设置baseurl等)
│  ├─http.api 			// 请求接口
│  ├─router 			// 路由文件
│  └─http.interceptor 	// 拦截器
├─components
│  ├─cl-test			// easycom测试组件 (示例使用可删除)
│  ├─mp-icon			// iconfont图标库
│  └─mp-html			// 富文本组件
├─store			
│  ├─$u.mixin			// 混入方法
│  └─index				// vuex
└─uview-ui				// uview
```


###	Iconfont 注意事项
1. 在阿里矢量图标库创建项目
2. 添加你所需要的图标后修改图标名称为 icon-*
3. 下载文件到本地后替换mp-icon里面的文件
4. 使用方法如下type内只需填写icon-* 后面的 *


### Iconfont示例
	
iconfont库中修改示例:

![avatar](https://ae01.alicdn.com/kf/U04de55f240c3496289865f7d18e0fc6fg.jpg)

代码示例:

```
	<mp-icon type="test" size="30" color="#000000"></mp-icon>
```


### 依赖安装

安装之前请移除下载后自带的package.json 使用下面代码安装依赖即可运行

```
npm i node-sass sass-loader uni-simple-router uni-read-pages
```


[gitee](https://gitee.com/ChenMuShan/uniapp-template)