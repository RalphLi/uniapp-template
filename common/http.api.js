// common/http.api.js

// api 接口管理
const install = (Vue, vm) => {

	vm.$u.api = {
		// test api
		getSearch: (params = {}) => vm.$u.get('/ebapi/store_api/hot_search', {
			id: 2
		}),
		
		// test api
		postSearch: (params = {}) => vm.$u.post('/ebapi/store_api/hot_search', params)
	};
}

export default {
	install
}
