const CONFIG = {
	// 开发环境配置
	development: {
		baseUrl: 'https://api.example.com',
	},
	// 生产环境配置
	production: {
		baseUrl: 'https://api.example.com',
	}
}
export default CONFIG[process.env.NODE_ENV];
